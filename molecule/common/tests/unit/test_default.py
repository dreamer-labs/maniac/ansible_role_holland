import os
import pytest

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']
).get_hosts('all')


def test_haproxy_config_file(host):
    f = host.file('/etc/holland/providers/mysqldump-lvm.conf')
    assert f.exists


@pytest.mark.parametrize('pkg', [
  'holland-mysqldump',
  'holland',
  'holland-mysqllvm'
])
def test_pkg(host, pkg):
    package = host.package(pkg)

    assert package.is_installed
