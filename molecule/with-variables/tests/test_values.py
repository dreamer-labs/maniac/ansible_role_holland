import os
import pytest

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']
).get_hosts('all')


@pytest.mark.parametrize('content', [
  'holland_borg_back',
  'holland_borg_pass'
])
def test_mysql_conf1(host, content):
    file = host.file("/etc/holland/providers/mysqldump-lvm.conf")

    assert file.contains(content)


@pytest.mark.parametrize('content', [
  'exclude-databases  = "mysql"',
  'holland_tests'
])
def test_default_conf2(host, content):
    file = host.file("/etc/holland/backupsets/default.conf")

    assert file.contains(content)


@pytest.mark.parametrize('content', [
  '/var/spool/holland'
])
def test_holland_conf3(host, content):
    file = host.file("/etc/holland/holland.conf")

    assert file.contains(content)
